# Data-driven large-scale genomic analysis reveals an intricate phylogenetic and functional landscape in J-domain proteins

## Scope
This repository contains the codebase accompagnying the manuscript **"Data-driven large-scale genomic analysis reveals an intricate phylogenetic and functional landscape in J-domain proteins"**, by Malinverni et al, 2023.

[Link to the preprint](https://www.biorxiv.org/content/10.1101/2022.02.28.482344v4)

[Link to the paper](https://www.pnas.org/doi/abs/10.1073/pnas.2218217120)

## Getting started
This repository contains code to reproduce the ANN-based analysis presented in the paper and generalize to other protein families.

It also contains the dataset as used in the paper under dataset/dataset_filtered_paper.csv.

The two main components are the generation of the dataset and the training of ANNs to perform several classification tasks.

Adaptating the code for other protein families will require modifying some family-specific lines in the dataset preparation scripts. Example include changing the InterPro target family name and domain name in getDataset.py and adapting the dataset filtering steps in filterDataset.
## Requirements
This codebase requires relatively standard datascience python packages, listed in requirements.txt.

It also requires the installation of the lbsNN package for training and using sequence-based ANNs. This package is available at https://gitlab.com/LBS-EPFL/code/lbsNN.

Once cloned, please add the contents of the bin path to you PATH, and the contents of lbsNN to your PYTHONPATH. These steps are achieved by

```
git clone https://gitlab.com/LBS-EPFL/code/lbsNN
cd lbsNN
echo 'export PATH=$PATH:$PWD/bin' >> ~/.bashrc
echo 'export PYTHONPATH=$PYTHONPATH:$PWD/lbsNN' >> ~/.bashrc
```

## Dataset preparation
The dataset preparation routines include generating the initial dataset by querying Uniprot and InterPro for the target family, filtering of the dataset and preparing the dataset for ANN analysis (train/validation/test split, weights computation, removal of model organism).

These steps can be performed with the following scripts:

1. ``` ./src/dataset_preparation/getDataset.py ```

2. ``` ./src/dataset_preparation/filterDataset.py ```

3. ``` ./src/dataset_preparation/makeMLDataset.py ```

4. ``` ./src/dataset_preparation/formatDataset.sh taskName```

5. ``` ./src/dataset_preparation/writeWeights.sh taskName outPath```

## ANN analysis
The ANN analysis routines include fitting models with hyper-parameter grid search, selection of the best models and prediction of target organisms.

These steps can be performed with the following scripts:
     
1. ``` ./src/ann/fitAllModels.sh task ```

2. ``` ./src/ann/selectBestModels.sh task ```

3. ``` ./src/ann/predictOrganisms.sh task fastaFile model_h5_file outFile ```