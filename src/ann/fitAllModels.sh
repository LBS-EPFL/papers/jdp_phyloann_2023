#!/bin/bash

export PATH=$PATH:/Users/dmalinve/Work/Codes/lbsNN/bin

task=$1

for layers in {2,3,4};
do
    for nodes in {32,64,128};
    do
	for reg in {0.0000001,0.000001,0.00001,0.0001};
	do
	    for dropout in {0.1,0.2};
	    do
		for lrate in {0.2,0.3,0.4,0.5};
		do
		    modelName=models/model_${task}l_${layers}_n_${nodes}_reg_${reg}_drop_${dropout}_lr_${lrate}
		    echo $modelName
		    lbsnn-fit -verbose -x analysis/ann/${task}/dataset_ML_${task}_train_JD_encoded.npy -y analysis/ann/${task}/dataset_ML_${task}_train_label_encoded.npy \
			      -valx analysis/ann/${task}/dataset_ML_${task}_val_JD_encoded.npy -valy analysis/ann/${task}/dataset_ML_${task}_val_label_encoded.npy \
			      --sample-weights analysis/ann/${task}/dataset_ML_${task}_train_weights.dat \
			      --val-sample-weights analysis/ann/${task}/dataset_ML_${task}_val_weights.dat \
			      -l $layers -u $nodes -br "l2($reg)" -kr "l2($reg)" --dropout $dropout --balance\
			      -pt 100 -ep 1000 --optimizer Adam --learning-rate $lrate --learning-decay 0.01 -m val_loss --outfolder $modelName 
		done
	    done
	    
	done
	
    done
done


