#!/bin/bash

export PATH=$PATH:/Users/dmalinve/Work/Codes/lbsNN/bin

task=$1
layers=$2
nodes=$3
reg=$4
dropout=$5
lrate=$6

modelName=models/model_${task}l_${layers}_n_${nodes}_reg_${reg}_drop_${dropout}_lr_${lrate}

lbsnn-fit -verbose -x analysis/ann/${task}/dataset_ML_${task}_train_JD_encoded.npy -y analysis/ann/${task}/dataset_ML_${task}_train_label_encoded.npy \
	  -valx analysis/ann/${task}/dataset_ML_${task}_val_JD_encoded.npy -valy analysis/ann/${task}/dataset_ML_${task}_val_label_encoded.npy \
          --sample-weights analysis/ann/${task}/dataset_ML_${task}_train_weights.dat --val-sample-weights analysis/ann/${task}/dataset_ML_${task}_val_weights.dat \
	  -l $layers -u $nodes -br "l2($reg)" -kr "l2($reg)" --dropout $dropout --balance\
	  -pt 100 -ep 1000 --optimizer Adam --learning-rate $lrate --learning-decay 0.01 -m val_loss --outfolder $modelName 
