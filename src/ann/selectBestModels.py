#!/usr/bin/env python3

import glob
import pandas as pd

if __name__=="__main__":

    tasks=['AB','ABC','Localization','PhyloBac','PhyloEuk','PhyloMeta','PhyloKingdom','Top12Archs']
    for task in tasks:
        models=glob.glob('models/model_'+task+'l_*')
        params=[]
        val_losses=[]
        val_acc=[]
        for m in models:
            if glob.glob(m+'/traininglog.0.csv'):
                perf=pd.read_csv(m+'/traininglog.0.csv')
                val_losses.append(perf.val_loss.iloc[-1])
                val_acc.append(perf.val_accuracy.iloc[-1])
                params.append(m)
            else:
                print(m," is missing")
        df=pd.DataFrame({'model':params,'val_loss':val_losses,'val_accuracy':val_acc})
        print(task,df.shape[0])
        if df.shape[0]>0:
            print('Val_accuracy:',df.sort_values('val_accuracy').iloc[-1].model,df.sort_values('val_accuracy').iloc[-1].val_loss,df.sort_values('val_accuracy').iloc[-1].val_accuracy)
            print('Val_loss:',df.sort_values('val_loss').iloc[0].model,df.sort_values('val_loss').iloc[0].val_loss,df.sort_values('val_loss').iloc[0].val_accuracy)
        
