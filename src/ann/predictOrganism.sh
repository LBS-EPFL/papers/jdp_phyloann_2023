#!/bin/bash

export PATH=$PATH:/Users/dmalinve/Work/Codes/lbsNN/bin

Task=$1
orgFasta=$2
model=$3
outFile=$4

# Encode sequences
fst-encode  $orgFasta  analysis/ann/${Task}/dataset_ML_${Task}_JD.profile target.npy

# Predict
lbsnn-predict -x target.npy -m $model -o $outFile -p  analysis/ann/${Task}/dataset_ML_${Task}_label.profile

echo $Task
if [[ $Task == 'ABC' ]]
then
    echo "abc"
    grep ">" $orgFasta | sed 's/>//g' > __tmp
    python3 -c "import numpy as np; x=np.loadtxt('$outFile',dtype=object)[:,1:].astype(float).round(2).astype(str).astype(object);y=x[:,0]+'/'+x[:,1]+'/'+x[:,2];np.savetxt('__tmp.dat',y,fmt='%s')"
    paste __tmp __tmp.dat > $outFile
    rm __tmp __tmp.dat
    
elif [[ $Task == 'Top12Archs' ]]
then
    echo "archs"
    grep ">" $orgFasta | sed 's/>//g' > __tmp
    python -c "import numpy as np; x=np.loadtxt('$outFile',dtype=object);y=np.concatenate((x[:,:1],x[:,1:].astype(float).max(axis=1,keepdims=True).round(2)),axis=1).astype(str).astype(object);z=y[:,0]+' ('+y[:,1]+')';np.savetxt('__tmp.dat',z,fmt='%s')"
    paste __tmp __tmp.dat > $outFile
    rm __tmp __tmp.dat
fi

# Clean up
rm target.npy 
