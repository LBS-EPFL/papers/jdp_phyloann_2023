#!/bin/bash


export PATH=$PATH:/Users/dmalinve/Work/Codes/lbsNN/bin

Task=$1
Sn=128

cat dataset/ML/${Task}/dataset_ML_${Task}_{train,val,test}.fasta >__tmp_all.fasta
fst-profile __tmp_all.fasta > analysis/ann/${Task}/dataset_ML_${Task}_JD.profile

cat dataset/ML/${Task}/dataset_ML_${Task}_{train,val,test}_label.txt |sed 's/ /_/g' >__tmp_all.txt
lbsnn-profile __tmp_all.txt > analysis/ann/${Task}/dataset_ML_${Task}_label.profile

for type in {train,val,test}
do
    # Encode sequences
    fst-encode  dataset/ML/${Task}/dataset_ML_${Task}_${type}.fasta analysis/ann/${Task}/dataset_ML_${Task}_JD.profile analysis/ann/${Task}/dataset_ML_${Task}_${type}_JD_encoded.npy

    #Encode target labels
    lbsnn-encode  -fl S${Sn} dataset/ML/${Task}/dataset_ML_${Task}_${type}_label.txt analysis/ann/${Task}/dataset_ML_${Task}_label.profile analysis/ann/${Task}/dataset_ML_${Task}_${type}_label_encoded.npy
done

rm __tmp_all.fasta __tmp_all.txt
