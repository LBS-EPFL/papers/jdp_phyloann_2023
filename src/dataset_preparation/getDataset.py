#!/usr/bin/env python3

""" Requires the following
    -import biopython
    -targetP (set path below)
"""
import io
import re
import os
import tqdm
import requests
import bisect
import subprocess
import pandas as pd
import Bio.SeqIO
import Bio.SearchIO

if __name__=="__main__":

    # Get all Uniprot entries containing annotated InterPRO DnaJ domain
    print("Getting all DnaJ entries from Uniprot")
    res=requests.get("https://legacy.uniprot.org/uniprot/?query=database:(type:interpro%20IPR001623)&format=tab&columns=id,database(pfam),database(interpro),lineage(ALL),sequence")
    df=pd.read_csv(io.StringIO(res.text),delimiter='\t')
    df.to_csv('dataset_unip.csv',index=False)

    # Align sequences to the DnaJ PFAM model, using hmmer
    print("Aligning sequences to DnaJ hmm mdoel (PFAM)")
    df=pd.read_csv('dataset_unip.csv')
    with open('__tmp.fasta','w') as f:
        for i,s in df.iterrows():
            f.write('>'+s.Entry+'\n')
            f.write(s.Sequence+'\n')
        
    subprocess.run(['hmmalign','--outformat','A2M','--trim','-o','__tmp_ali.fasta','dataset/DnaJ.hmm','__tmp.fasta'],stdout=subprocess.DEVNULL)
    seqs=Bio.SeqIO.parse('__tmp_ali.fasta','fasta')
    alignedSeqs=[''.join([x for x in s.seq if not x.islower()]).replace('.','') for s in seqs]
    os.remove("__tmp.fasta")
    os.remove("__tmp_ali.fasta")
    
    df['JD_sequence']=alignedSeqs
    df.to_csv('dataset_jd.csv',index=False)
    
    # Retrieve full architectures from Interpro, including domain architecture order
    print("Retrieving domain organization from InterPRO")
    df=pd.read_csv('dataset_jd.csv')
    INTERPRO_domains=['']*df.shape[0]
    domainStarts=['']*df.shape[0]
    JD_start=[0]*df.shape[0]
    JD_end=[0]*df.shape[0]

    session=requests.Session()
    for i,s in tqdm.tqdm(df.iterrows(),total=df.shape[0]):
        for attempt in range(3):
            try:
                r=session.get("https://www.ebi.ac.uk/interpro/api/entry/interpro/protein/uniprot/"+s.Entry)
                hits=[d for d in r.json()['results'] if d['metadata']['type']in ['domain','repeat']]
                doms=[]
                st=[]
                for match in hits:
                    for p in [prot['entry_protein_locations'] for prot in match['proteins']][0]:
                        domainName=match['metadata']['name'].replace(' ','_')
                        doms.append(domainName)
                        st.append(int(p['fragments'][0]['start']))
                        if domainName=='DnaJ_domain':
                            JD_start[i]=st[-1]
                            JD_end[i]=int(p['fragments'][0]['end'])
                INTERPRO_domains[i]=';'.join([d for _,d in sorted(zip(st,doms))])
                domainStarts[i]=';'.join([str(s) for s in sorted(st)])
            except:
                print(s.Entry," missed.")
                INTERPRO_domains[i]='NA'
                continue
            break

    df['Domains']=INTERPRO_domains
    df['DomainsStart']=domainStarts
    df['JD_start']=JD_start
    df['JD_end']=JD_end
    df.to_csv('dataset_domains.csv',index=False)

    
    # Identify G/F region, i.e. 40 AA after end of JD in if that region contains at least 25% of G/F.
    print("Assigning G/F regions")
    df=pd.read_csv('dataset_domains.csv')
    GF=['']*df.shape[0]
    for i,s in df.iterrows():
        if s.JD_start>0 and s.JD_end+40<=len(s.Sequence)+1 and (((s.Sequence[s.JD_end:(s.JD_end+40)].count('G')+s.Sequence[s.JD_end:(s.JD_end+40)].count('F'))/40) > 0.25):
            GF[i]=s.Sequence[s.JD_end:(s.JD_end+40)]            
    df['GF']=GF
    df.to_csv('dataset_gf.csv',index=False)
                                 
    # Compute subcellular localization by TargetP
    print("Computing subcellular localizaion by TargetP")
    targetP='/Users/dmalinve/Dropbox/Work/papers/2023_JDM/JDP_data2023/codes/targetp-2.0/bin/targetP'        
    df=pd.read_csv('dataset_gf.csv')
    with open('__tmp.fasta','w') as f:
        for i,s in df.iterrows():
            f.write(">"+s.Entry+"\n"+s.Sequence+"\n")
    subprocess.run([targetP,'-fasta','__tmp.fasta','-org','pl',
                    '-format','short'],stdout=subprocess.DEVNULL)
    loc=pd.read_csv('__tmp_summary.targetp2',delimiter='\t',header=None,comment="#")
    df['Localization']=loc[1]
    os.remove("__tmp.fasta")
    os.remove("__tmp_summary.targetp2")
    df.to_csv('dataset_localization.csv',index=False)

    # Align sequences to the pseudoZFLR PFAM model, using pyhmmer pipeline and add hits to domain architectures in order
    print("Aligning sequences to pseudo-ZFLR HMM (manual)")
    Evalue=0.0001
    df=pd.read_csv('dataset_localization.csv')
    with open('__tmp.fasta','w') as f:
        for i,s in df.iterrows():
            f.write('>'+s.Entry+'\n')
            f.write(s.Sequence+'\n')
    subprocess.run(['hmmsearch','-E','0.0001','-o','__tmp.hits','analysis/pseudo-ZFLR/pseudo-ZFLR.hmm','__tmp.fasta'],stdout=subprocess.DEVNULL)
    for q in Bio.SearchIO.parse('__tmp.hits','hmmer3-text'):
        for hit in tqdm.tqdm(q.hits):
            for dom in hit.hsps:
                positions=[int(s) for s in df[df.Entry==hit.id].iloc[0].DomainsStart.split(';')]
                domains=df[df.Entry==hit.id].iloc[0].Domains.split(';')
                insertPosition=bisect.bisect(positions,dom.hit_start)
                positions.insert(insertPosition,dom.hit_start)
                domains.insert(insertPosition,"pseudoZFLR")
                df.loc[df.Entry==hit.id,'Domains']=';'.join(domains)
                df.loc[df.Entry==hit.id,'DomainsStart']=';'.join([str(s) for s in positions])
    os.remove("__tmp.fasta")
    os.remove("__tmp.hits")
    df.to_csv('dataset_pzflr.csv',index=False)
  
        
    # Group TPR-repeats into a single effectie domain. So all TPR_i will be grouped as a single TPR domain
    # Becauase there is no 1-1 order preserving map, TPR grouped architectures are alphabetically ordered by their domain names
    # E.g. TPR_1;DnaJ;TPR_17 --> DnaJ;TPR
    # E.g. TPR_3;DnaJ;TPR_3;AA9 --> AA9;DnaJ;TPR
    # Add a column called originalDomains to keep track of the original domain names
    print("Grouping TPR architectures")
    df=pd.read_csv('dataset_pzflr.csv').fillna('')
    df['originalDomains']=df.Domains
    tprDomains=df[df.Domains.str.contains('Tetratricopeptide_repeat')]
    df.loc[df.Domains.str.contains('Tetratricopeptide_repeat'),'Domains']=tprDomains.Domains.map(lambda x: ';'.join(sorted(['Tetratricopeptide_repeat']+list(filter(('Tetratricopeptide_repeat').__ne__,re.sub(r'Tetratricopeptide_repeat_[0-9]*','Tetratricopeptide_repeat',x).split(';'))))))
    df.to_csv('dataset_tpr.csv')

        
    # Assign classes: ABC, Top10Archs,PhyloDomain, PhyloKingdom, Loc
    print("Assigning classes")
    df=pd.read_csv('dataset_tpr.csv')
    abc=['']*df.shape[0]
    topArchs=['']*df.shape[0]
    phyloBac=['']*df.shape[0]
    phyloEuk=['']*df.shape[0]
    phyloMeta=['']*df.shape[0]
    phyloKing=['']*df.shape[0]
    loc=['']*df.shape[0]
    nTopArchs=12
    _topArchs=list(df.Domains.value_counts().index[:nTopArchs])
    _loc=['SP','mTP','cTP']
    for i,s in df.iterrows():
        doms=s.Domains
        if doms=='DnaJ_domain;Chaperone_DnaJ,_C-terminal;Heat_shock_protein_DnaJ,_cysteine-rich_domain':
            abc[i]='A'
        elif doms=='DnaJ_domain;Chaperone_DnaJ,_C-terminal':
            abc[i]='B'
        else:
            abc[i]='C'
                
        topArchs[i]=doms if doms in _topArchs else 'Other'
        loc[i]=s.Localization if s.Localization in _loc else 'Other'
            
        if 'FCB' in s['Taxonomic lineage (ALL)']:
            phyloBac[i]='FCB'
        elif 'Terrabacteria group' in s['Taxonomic lineage (ALL)']:
            phyloBac[i]='Terrabacteria_group'
        elif 'PVC group' in s['Taxonomic lineage (ALL)']:
            phyloBac[i]='PVC_group'
        elif 'Proteobacteria' in s['Taxonomic lineage (ALL)']:
            phyloBac[i]='Proteobacteria'
        elif 'Bacteria' in s['Taxonomic lineage (ALL)']:
            phyloBac[i]='Other_Bacteria'

                
        if 'Fungi' in s['Taxonomic lineage (ALL)']:
            phyloEuk[i]='Fungi'
        elif 'Metazoa' in s['Taxonomic lineage (ALL)']:
            phyloEuk[i]='Metazoa'
        elif 'Viridiplantae' in s['Taxonomic lineage (ALL)']:
            phyloEuk[i]='Viridiplantae'
        elif 'Eukaryota' in s['Taxonomic lineage (ALL)']:
            phyloEuk[i]='Other_Eukaryotes'

        if 'Protostomia' in s['Taxonomic lineage (ALL)']:
            phyloMeta[i]='Protostomia'
        elif 'Mammalia' in s['Taxonomic lineage (ALL)']:
            phyloMeta[i]='Mammalia'
        elif 'Sauropsida' in s['Taxonomic lineage (ALL)']:
            phyloMeta[i]='Sauropsida'
        elif 'Metazoa' in s['Taxonomic lineage (ALL)']:
            phyloMeta[i]='Other_Metazoans'

        if 'Bacteria' in s['Taxonomic lineage (ALL)']:
            phyloKing[i]='Bacteria'
        elif 'Eukaryota' in s['Taxonomic lineage (ALL)']:
            phyloKing[i]='Eukaryota'
        elif 'Archaea' in s['Taxonomic lineage (ALL)']:
            phyloKing[i]='Archaea'
        elif 'Viruses' in s['Taxonomic lineage (ALL)']:
            phyloKing[i]='Viruses'
        else:
            phyloKing[i]='Other'
                
    df['labelABC']=abc
    df['labelTopArchs']=topArchs
    df['labelPhyloBac']=phyloBac
    df['labelPhyloEuk']=phyloEuk
    df['labelPhyloMeta']=phyloMeta
    df['labelPhyloKing']=phyloKing
    df['labelLoc']=loc
    
    df.to_csv('dataset_full.csv',index=False)
