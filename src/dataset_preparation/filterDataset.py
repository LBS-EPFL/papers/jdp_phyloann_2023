#!/usr/bin/env python3
import pandas as pd

if __name__=="__main__":

    df=pd.read_csv('dataset_full.csv')
    print("Initial number of sequences in dataset:",df.shape[0])

    # Remove sequences without annotated DnaJ domain in InterPro
    df=df[(df.Domains=='DnaJ_domain')|df.Domains.str.startswith('DnaJ_domain;')|
           df.Domains.str.endswith(';DnaJ_domain')|df.Domains.str.contains(';DnaJ_domain;')]
    print("Number of sequences with annotated DnaJ in InterPro:",df.shape[0])
    
    # Remove sequences without HPD motif or HPE
    df=df[(df.JD_sequence.str[28:31]=='HPD')|(df.JD_sequence.str[28:31]=='HPE')]
    print("Number of sequences with full HPD/HPE:",df.shape[0])

    # Remove sequences with multiple J-domains
    df=df[df.Domains.map(lambda x :x.split(";").count("DnaJ_domain"))==1]
    print("Number of sequences with only one J-domain:",df.shape[0])

    # Remove sequences with unclassified organism
    df=df[~df['Taxonomic lineage (ALL)'].str.contains('unclassified entries')]
    print("Number of sequences with identified organism:",df.shape[0])
    
    df.to_csv('dataset_filtered.csv',index=False)
