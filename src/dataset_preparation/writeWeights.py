#!/usr/bin/env python3

import sys
import numpy as np
import pandas as pd

def getIds(fasta):
    ids=[]
    with open(fasta,'r') as f:
        for l in f:
            if l.startswith(">"):
                ids.append(l.strip().replace('>',''))
    return ids

if __name__=="__main__":
    case=sys.argv[1]
    out=sys.argv[2]
    df=pd.read_csv(case)
    
    train=case[:-4]+'_train.fasta'
    val=case[:-4]+'_val.fasta'
    test=case[:-4]+'_test.fasta'
    
    idx=getIds(train)
    ws=df[df.Entry.isin(idx)]
    ws=ws.reindex(ws.Entry.map({x: i for i, x in enumerate(idx)}).sort_values().index).ws
    np.savetxt(out+'/'+case[:-4].split('/')[-1]+'_train_weights.dat',ws)

    idx=getIds(val)
    ws=df[df.Entry.isin(idx)]
    ws=ws.reindex(ws.Entry.map({x: i for i, x in enumerate(idx)}).sort_values().index).ws
    np.savetxt(out+'/'+case[:-4].split('/')[-1]+'_val_weights.dat',ws)

    idx=getIds(test)
    ws=df[df.Entry.isin(idx)]
    ws=ws.reindex(ws.Entry.map({x: i for i, x in enumerate(idx)}).sort_values().index).ws
    np.savetxt(out+'/'+case[:-4].split('/')[-1]+'_test_weights.dat',ws)
    
