#!/usr/bin/env python3
import os
import tqdm
import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist,pdist,squareform
import time
import matplotlib.pyplot as plt

def computeSeqWeights(dataset,maxID=0.80):
    """ Compute sequence weights, reweighting sequences with pariwise identity above maxID inversely-proportional to their number (i.e. DCA-like reweighting) """
    dc='-ACDEFGHIKLMNPQRSTVWY'
    JDs=dataset.JD_sequence
    JDs=np.asarray([[dc.index(aa) for aa in s.replace('X','-')] for s in JDs])

    print("Computing sequence weights")
    ws=np.zeros(dataset.shape[0])
    for i in tqdm.tqdm(range(len(ws))):
        ws[i]=1./(((1.-cdist([JDs[i]],JDs,'hamming'))>=maxID).sum())
    return ws
              
def splitMLDataset(dataset,trainFrac,valFrac):
    """ Split a dataset in train, validation and test set, so that all sequences of an organism fall in the same set, and that the spit-fractions are basead on the weights"""

    dataset['OS']=dataset['Taxonomic lineage (ALL)'].map(lambda x:x.split(',')[-1])
    orgs=dataset.OS.unique()
    np.random.shuffle(orgs)

    trSet=pd.DataFrame(columns=dataset.columns)
    valSet=pd.DataFrame(columns=dataset.columns)
    testSet=pd.DataFrame(columns=dataset.columns)
    print("Splitting dataset according to organism and sample weights.")
    
    for org in tqdm.tqdm(orgs):
        if (trSet.ws.sum()/dataset.ws.sum())< trainFrac:
            trSet=trSet.append(dataset[dataset.OS==org])
        elif (valSet.ws.sum()/dataset.ws.sum())< valFrac:
            valSet=valSet.append(dataset[dataset.OS==org])
        else:
            testSet=testSet.append(dataset[dataset.OS==org])

    return {'train':trSet,'val':valSet,'test':testSet}

if __name__=="__main__":

    outFolder='.'
    
    df=pd.read_csv('dataset_filtered.csv').fillna('')

    #### Remove sequences from ML training set
    
    print("Initial number of sequences in dataset:",df.shape[0])
    # Remove sequences from model organism: H. Sapiens, M. Musculus, S. Cerevisiae,
    # S. Pombe, R. Norvegicus, D. Melanogaster, D. Rerio, C. Elegans, A. Thaliana
    sp=df['Taxonomic lineage (ALL)'].map(lambda x:x.split(',')[-1])
    df=df[(~sp.str.contains("Homo sapiens"))&(~sp.str.contains("Saccharomyces cerevisiae"))&\
          (~sp.str.contains("Danio rerio"))&(~sp.str.contains("Arabidopsis thaliana"))&\
          (~sp.str.contains("Caenorhabditis elegans"))&(~sp.str.contains("Rattus norvegicus"))&\
          (~sp.str.contains("Drosophila melanogaster"))
          ]
    print("Number of sequences without target organisms:",df.shape[0])

    # Remove non-canonical class B sequences, i.e. have JD, identified G/F but no DnaJ
    df=df[~(((df.GF.map(len))>0)&(df.Domains.map(lambda x: 'Chaperone_DnaJ,_C-terminal' not in x.split(';'))))]
    print("Number of sequences without non-canonical class Bs:",df.shape[0])
    exit(1)
    #### Save ML ready dataset as csv, fasta and one-colum target-label files. Split in train,validation and test set for each task

    # A/B, only class A and (canonical) B are classified
    os.mkdir(outFolder+'AB')
    d=df[(df.labelABC=='A')|(df.labelABC=='B')].copy()
    wsAB =computeSeqWeights(d,0.8)
    d['ws']=wsAB
    d.to_csv(outFolder+'AB/dataset_ML_AB.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'AB/dataset_ML_AB_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelABC.to_csv(outFolder+'AB/dataset_ML_AB_'+setType+'_label.txt',index=False,header=None)

        
    # A/B/C, all sequences
    os.mkdir(outFolder+'ABC')
    d=df.copy()
    wsABC=computeSeqWeights(d,0.8)
    d['ws']=wsABC
    d.to_csv(outFolder+'ABC/dataset_ML_ABC.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'ABC/dataset_ML_ABC_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelABC.to_csv(outFolder+'ABC/dataset_ML_ABC_'+setType+'_label.txt',index=False,header=None)
    
   

    # Localization: Only classify Mito, ER and Chloroplast in eukaryotic sequences, subset of sequences used
    os.mkdir(outFolder+'Localization')
    d=df[(df.labelLoc=='SP')|(df.labelLoc=='cTP')|(df.labelLoc=='mTP')].copy()
    wsLoc =computeSeqWeights(d,0.8)
    d['ws']=wsLoc
    d.to_csv(outFolder+'Localization/dataset_ML_Localization.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'Localization/dataset_ML_Localization_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelLoc.to_csv(outFolder+'Localization/dataset_ML_Localization_'+setType+'_label.txt',index=False,header=None)
    
    # PhyloKingdom: Classify at the higher taxonomic level (Bacteria, Eukaryota, Archaea, Viruses), all sequences used
    os.mkdir(outFolder+'PhyloKingdom')
    d=df.copy()
    d['ws']=wsABC
    d.to_csv(outFolder+'PhyloKingdom/dataset_ML_PhyloKingdom.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'PhyloKingdom/dataset_ML_PhyloKingdom_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelPhyloKing.to_csv(outFolder+'PhyloKingdom/dataset_ML_PhyloKingdom_'+setType+'_label.txt',index=False,header=None)

    # PhyloBac: Classify at the bacterial taxonomic level (), subset of sequences used
    os.mkdir(outFolder+'PhyloBac')
    d=df[(df.labelPhyloBac=='FCB')|(df.labelPhyloBac=='Terrabacteria_group')|(df.labelPhyloBac=='PVC_group')|
         (df.labelPhyloBac=='Proteobacteria')|(df.labelPhyloBac=='Other_Bacteria')].copy()
    wsPhyloBac=computeSeqWeights(d,0.8)
    d['ws']=wsPhyloBac
    d.to_csv(outFolder+'PhyloBac/dataset_ML_PhyloBac.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'PhyloBac/dataset_ML_PhyloBac_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelPhyloBac.to_csv(outFolder+'PhyloBac/dataset_ML_PhyloBac_'+setType+'_label.txt',index=False,header=None)

    # PhyloEuk: Classify at the eukaryotic taxonomic level (), subset of sequences used
    os.mkdir(outFolder+'PhyloEuk')
    d=df[(df.labelPhyloEuk=='Fungi')|(df.labelPhyloEuk=='Metazoa')|(df.labelPhyloEuk=='Viridiplantae')|
         (df.labelPhyloEuk=='Other_Eukaryotes')].copy()
    wsPhyloEuk=computeSeqWeights(d,0.8)
    d['ws']=wsPhyloEuk
    d.to_csv(outFolder+'PhyloEuk/dataset_ML_PhyloEuk.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'PhyloEuk/dataset_ML_PhyloEuk_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelPhyloEuk.to_csv(outFolder+'PhyloEuk/dataset_ML_PhyloEuk_'+setType+'_label.txt',index=False,header=None)
    
    # PhyloMeta: Classify at the metazoan taxonomic level (), subset of sequences used
    os.mkdir(outFolder+'PhyloMeta')
    d=df[(df.labelPhyloMeta=='Protostomia')|(df.labelPhyloMeta=='Mammalia')|(df.labelPhyloMeta=='Sauropsida')|
         (df.labelPhyloMeta=='Other_Metazoans')].copy()
    wsPhyloMeta=computeSeqWeights(d,0.8)
    d['ws']=wsPhyloMeta
    d.to_csv(outFolder+'PhyloMeta/dataset_ML_PhyloMeta.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'PhyloMeta/dataset_ML_PhyloMeta_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelPhyloMeta.to_csv(outFolder+'PhyloMeta/dataset_ML_PhyloMeta_'+setType+'_label.txt',index=False,header=None)
    
   
    # Top12Archs classes. Top11 most occurring + other
    os.mkdir(outFolder+'Top12Archs')
    d=df[df.labelTopArchs!='DnaJ_domain'].copy()
    wsTop12=computeSeqWeights(d,0.8)
    d['ws']=wsTop12
    d.to_csv(outFolder+'Top12Archs/dataset_ML_Top12Archs.csv')
    sets=splitMLDataset(d,0.7,0.15)
    for setType in ['train','val','test']:
        ds=sets[setType]
        with open(outFolder+'Top12Archs/dataset_ML_Top12Archs_'+setType+'.fasta','w') as f:
            for i,s in ds.iterrows():
                f.write('>'+s.Entry+'\n')
                f.write(s.JD_sequence+'\n')
        ds.labelTopArchs.to_csv(outFolder+'Top12Archs/dataset_ML_Top12Archs_'+setType+'_label.txt',index=False,header=None)
